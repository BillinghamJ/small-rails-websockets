class Websocket::EntityController < WebsocketRails::BaseController
	def initialize_session
		controller_store[:clients] = []
	end

	def client_disconnected
		WebsocketRails[:room].trigger(:user_left, { id: client_id })

		controller_store[:clients].delete_if { |x| x[:id] == client_id }
	end

	def create
		loc = message

		trigger_success({ clients: controller_store[:clients] })
		WebsocketRails[:room].trigger(:user_joined, { id: client_id, location: loc })

		controller_store[:clients].push({ id: client_id, location: loc })
	end

	def move
		loc = message

		WebsocketRails[:room].trigger(:user_moved, { id: client_id, location: loc })

		i = controller_store[:clients].index { |x| x[:id] == client_id }
		controller_store[:clients][i] = { id: client_id, location: loc }
	end
end
