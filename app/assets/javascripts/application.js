// This file is included in all sites and subsites.  This is a
// manifest file that'll be compiled into including all the files
// listed below.  Add new JavaScript/Coffee code in separate files in
// the application directory and they'll automatically be included.
// It's not advisable to add code directly here, but if you do, it'll
// appear at the bottom of the the compiled file.
//
//= require jquery
//= require jquery_ujs
//= require jquery-ui
//= require websocket_rails/main
//= require ./jquery-event-drag
//= require_tree ./application
var dispatcher = new WebSocketRails(window.location.hostname + ':' + window.location.port + '/websocket');
var channel = dispatcher.subscribe('room');
var myId;

dispatcher.on_open = function (data) {
	myId = data.connection_id;

	dispatcher.trigger('entity.create', { x: 480, y: 270 }, function (data) {
		for (var i = 0; i < data.clients.length; i++)
		{
			var client = data.clients[i];
			$('.field').append('<div class="entity" id="entity_' + client.id + '" style="left: ' + client.location.x + 'px; top: ' + client.location.y + 'px;"></div>');
		}
	});
};

channel.bind('user_joined', function (client) {
	$('.field').append('<div class="entity" id="entity_' + client.id + '" style="left: ' + client.location.x + 'px; top: ' + client.location.y + 'px;"></div>');

	if (client.id == myId)
	{
		var entity = $('#entity_' + myId);

		entity.css('background-color', '#0000ff');

		entity.drag(function (ev, dd) {
			$(this).css({ left: dd.offsetX, top: dd.offsetY });
			dispatcher.trigger('entity.move', { x: dd.offsetX, y: dd.offsetY });
		});
	}
});

channel.bind('user_left', function (data) {
	$('#entity_' + data.id).remove();
});

channel.bind('user_moved', function (data) {
	$('#entity_' + data.id).css({ left: data.location.x, top: data.location.y });
});
