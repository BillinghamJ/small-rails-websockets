WebsocketRails::EventMap.describe do
	subscribe :client_disconnected, 'websocket/entity#client_disconnected'

	namespace :entity do
		subscribe :create, 'websocket/entity#create'
		subscribe :move, 'websocket/entity#move'
	end
end
